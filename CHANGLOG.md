# CHANGELOG

## 3.1.0-0

### ADD

- 添加了 `kcp` 协议支持, 伪装类型是 `wechat-video`

## 3.0.0

使用 `typescript` 重写 `v2ray config` 生成, 并且把命令当做 `npm` 包发布了, 虽然没有什么破坏性的更改但毕竟重写心里还是有点慌的, 所以就发布了大版本

### ADD

- 添加 `VNEXT` , 内容是 [`V2rayN`](https://github.com/2dust/v2rayN/wiki/%E5%88%86%E4%BA%AB%E9%93%BE%E6%8E%A5%E6%A0%BC%E5%BC%8F%E8%AF%B4%E6%98%8E(ver-2)) 客户端的分享链接.    
  默认的话是直接从当前服务器访问, 有了这个的话流量会转到下一个服务器

## 2.2.0

### ADD

- 添加了 `ws` 配置, 对应的内部端口是 3005

## 2.1.0

### ADD

- 添加了 `v2ray` 原生代理模式, 用作转发国外主机的端口, 使用方法
  ```yml
  proxy:
    image: shynome/v2ray:2.1.0
    environment:
      PROXY_PORTS: '80-83'
      PROXY_PORTS: '80,81,82,83' # 两个配置是等价的
      PROXY_HOSTNAME: google.com
  ```