FROM v2ray/official

# v2ray config generator now publish as npm package
RUN apk add --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/main \
  # nodejs
  nodejs npm \
  # 可以设置时区
  tzdata

COPY rootfs /
RUN chmod +x /v2ray-start

# COPY .tags /tmp/.tags
COPY v2ray_generator.tgz /tmp/v2ray_generator.tgz
RUN set -e \
  # && npm_tag=$(cat /tmp/.tags | grep -o -E '[^,]+$') \
  && npm i -g /tmp/v2ray_generator.tgz

ENV NODE_ENV='production'

EXPOSE 3001-3009

CMD [ "/v2ray-start" ]
