import { Log } from "./V2ray/Log";
import { Path, Host, Port } from "./V2ray";

declare global {
  type Env = {
    PASSWORD: string
    VMESS_ID: string
    SERVER_NAME: string
    TLS_CRT: string
    TLS_KEY: string
    LOG_LEVEL: Log['loglevel']
    /**要代理的服务器端口 */
    PROXY_PORTS: string
    /**要代理的服务器ip地址或域名 */
    PROXY_HOSTNAME: string 
    /**使用 websocket 或 http2 的代理路径 */
    PROXY_PATH: Path
    /** V2rayN 的分享链接 */
    VNEXT: string
    /**是否可访问内部网络, 默认不能访问 */
    LOCAL_AREA_NETWORK: string
    /**是否开启 mux, 默认关闭*/
    MUX_ENABLE: string
  }
}
