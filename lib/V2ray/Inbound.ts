import { Protocol, Port, IP, Tag } from ".";
import { Inbound as ProtocolInbound } from "./Protocols";
import { StreamSettings, StreamSettingKey } from "./StreamSettings";

export interface Inbound<P extends keyof ProtocolInbound, N extends StreamSettingKey> {
  /**
   *  端口。接受的格式如下:
   * - 整型数值: 实际的端口号。
   * - 环境变量 (V2Ray 3.23+): 以"env:"开头，后面是一个环境变量的名称，如`"env:PORT"`。V2Ray 会以字符串形式解析这个环境变量。
   * - 字符串 (V2Ray 3.23+): 一个数值类型的字符串，如"1234"。
   */
  port: Port|string
  /**
   * 监听地址，只允许 IP 地址，默认值为`"0.0.0.0"`。
   */
  listen?: IP
  /**
   *  连接协议名称，可选的值见[协议列表](https://www.v2ray.com/chapter_02/02_protocols.html)。
   */
  protocol: P
  /**
   * 具体的配置内容，视协议不同而不同。
   */
  settings: ProtocolInbound[P]
  /**
   * [底层传输配置](https://www.v2ray.com/chapter_02/05_transport.html#%E5%88%86%E8%BF%9E%E6%8E%A5%E9%85%8D%E7%BD%AE)。
   */
  streamSettings?:StreamSettings<N>
  /**
   * 此传入连接的标识，用于在其它的配置中定位此连接。属性值必须在所有 tag 中唯一。
   */
  tag?:Tag
  /**
   * 识别相应协议的流量，并根据流量内容重置所请求的目标。
   * - 接受一个字符串数组，默认值为空。
   * - 可选值为 `"http"` 和 `"tls"`。
   * - (V2Ray 3.32+) 已弃用。请使用 `sniffing`。当指定了 `domainOverride` 而没有指定 `sniffing` 时，强制开启 `sniffing。`
   */
  domainOverride?: 'http'|'tls'[]
  /**
   * (V2Ray 3.32+): 尝试探测流量类型。
   */
  sniffing?:{
    /**
     * 是否开启流量探测。
     */
    enabled?: boolean
    /**
     * 当流量为指定类型时，按其中包括的目标地址重置当前连接的目标。
     * 可选值为 `"http"` 和 `"tls"`。
     */
    destOverride: 'http'|'tls'[]
  }
}