import { Path } from ".";
export interface Log {
  access: Path
  error: Path
  /**
   * - 其中"debug"记录的数据最多，"error"记录的最少；
   * - "none"表示不记录任何内容；
   * - 默认值为"warning"。
   */
  loglevel: 'debug'|'info'|'warning'|'error'|'none'
}