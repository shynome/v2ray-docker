import { IP, Tag, Protocol } from ".";
import { Outbound as ProtocolOutbound } from "./Protocols";
import { StreamSettingKey, StreamSettings } from "./StreamSettings";

type a = keyof ProtocolOutbound

/**
 * @inheritDoc https://www.v2ray.com/chapter_02/01_overview.html#outbound
 * ### 主传出连接配置
 * 主传出连接用于向远程网站或下一级代理服务器发送数据，可用的协议请见[协议列表](https://www.v2ray.com/chapter_02/02_protocols.html)。
 */
export interface Outbound<P extends keyof ProtocolOutbound,N extends StreamSettingKey=any> {
  /**
   * 用于发送数据的 IP 地址，当主机有多个 IP 地址时有效，
   * 默认值为 `"0.0.0.0"`
   */
  sendThrough?: IP 
  tag?: Tag
  /**
   * 连接协议名称，可选的值见[协议列表](https://www.v2ray.com/chapter_02/02_protocols.html)。
   */
  protocol: P
  /**
   * 具体的配置内容，视协议不同而不同。
   */
  settings: ProtocolOutbound[P]
  streamSettings?:StreamSettings<N>,
  proxySettings?:{ tag: Tag }
  /**
   * @inheritDoc https://www.v2ray.com/chapter_02/mux.html
   * ### Mux 多路复用
   * Mux 功能是在一条 TCP 连接上分发多个 TCP 连接的数据。实现细节详见 [Mux.Cool](https://www.v2ray.com/developer/protocols/muxcool.html)
   * ##### 小贴士
   * - Mux 只需要在客户端启用，服务器端自动适配。
   * - 一条 TCP 连接最多传输 128 条连接之后关闭；
   * - Mux 是为了减少 TCP 的握手延迟而设计，而非提高连接的吞吐量。使用 Mux 看视频、下载或者测速通常都有反效果。
   */
  mux?: {
    /**
     * 是否启用 Mux , 默认 `false`
     */
    enabled?: boolean
    /**
     * 最大并发连接数
     * - default: 8
     * - min: 1
     * - max: 1024
     */
    concurrency?: number
  }
}