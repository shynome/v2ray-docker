import { Host, Port, Network, UserLevel, } from "..";

export interface DokodemoDoorInbound {
  /**
   * 指定服务器的地址，可以是一个 IPv4、IPv6 或者域名，字符串类型。
   * - 当 `followRedirect`（见下文）为 `true` 时，`address` 可为空。
   */
  address: Host
  /**
   * 指定服务器的端口，数值类型。
   */
  port: Port
  /**
   * 指定服务器的网络协议类型，可选值为“tcp”或“udp”。
   */
  network: Network
  /**
   * (V2Ray 3.1 后等价于对应用户等级的 `connIdle` 策略): 传入数据的时间限制（秒），默认值为 300。
   */
  timeout: number
  /**
   * 当值为 `true` 时，dokodemo-door 会识别出由 iptables 转发而来的数据，并转发到相应的目标地址。
   * - 目前只支持 Linux。
   * - 支持 TCP/IPv4 连接。
   * - 支持 UDP/IPv4 连接，需要 root (CAP_NET_ADMIN) 权限。
   */
  followRedirect: boolean
  /**
   * 用户等级，所有连接都会使用这个用户等级。
   */
  userLevel?: UserLevel
}

/**
 * ### Dokodemo-door
 * Dokodemo door（任意门）是一个传入数据协议，它可以监听一个本地端口，并把所有进入此端口的数据发送至指定服务器的一个端口，从而达到端口映射的效果。
 */
export interface DokodemoDoor {
  _name: 'dokodemo-door'
  Inbound: DokodemoDoorInbound
}
