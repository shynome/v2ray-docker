import { UserLevel, Hostname } from "..";

export interface FreedomOutbound {
  /**
   * 域名解析策略，可选的值有：
   * - `"AsIs"`: 默认值。不作更改，由操作系统解析。
   * - `"UseIP"`: 使用 V2Ray 的 [DNS 服务器](https://www.v2ray.com/chapter_02/04_dns.html)解析成 IP 之后再发送数据。
   */
  domainStrategy?: 'AsIs'|'UseIP'
  /**
   * (V2Ray 3.1 后等价于对应用户等级的 `connIdle` 策略): 从目标服务器读取响应数据的时限，单位为秒。默认值为 `300`。
   */
  timeout?: number
  /**
   * 将所有数据发送到指定地址（而不是传入协调指定的地址）。其值为一个字符串，样例：`"127.0.0.1:80"`, `":1234"`。
   * - (V2Ray 3.31+)当地址不指定时，如":443"，Freedom 不会修改原先的目标地址。
   * - (V2Ray 3.31+)当端口为0时，如"v2ray.com:0"，Freedom 不会修改原先的端口。
   */
  redirect?: Hostname
  userLevel?: UserLevel
}

/**
 * ### Freedom
 * Freedom 是一个传出数据协议，可以用来向任意网络发送（正常的） TCP 或 UDP 数据。
 */
export interface Freedom {
  _name: 'freedom'
  Outbound: FreedomOutbound
}