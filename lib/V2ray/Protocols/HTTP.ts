import { UserLevel } from "..";

export interface HTTPInbound {
  /**
   * (V2Ray 3.1 后等价于对应用户等级的 connIdle 策略): 从客户端读取数据的超时设置（秒），0 表示不限时。默认值为 300。
   */
  timeout?: number
  /**
   * 一个数组，数组中每个元素为一个用户帐号，用户名由user指定，密码由pass指定。默认值为空。
   * - 当 `accounts` 非空时，HTTP 代理将对传入连接进行 Basic Authentication 验证。
   */
  accounts?: { user: string, pass: string }[]
  /**
   * 当为`true`时，会转发所有 HTTP 请求，而非只是代理请求。若配置不当，开启此选项会导致死循环。
   */
  allowTransparent?: boolean
  userLevel?: UserLevel
}

/**
 * @inheritDoc https://www.v2ray.com/chapter_02/protocols/http.html
 * ### HTTP
 * HTTP 是一个传入数据协议，兼容 HTTP 1.x 代理。
 */
export class HTTP {
  _name: 'http'
  Inbound: HTTPInbound
}