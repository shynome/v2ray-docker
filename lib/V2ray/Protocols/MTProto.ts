import { UserLevel, Email } from "..";

export interface MTProtoInbound {
  /**
   * 一个数组，其中每一个元素表示一个用户。目前只有第一个用户会生效。
   */
  users?: {
    "email": Email,
    /**
     * 用户等级。
     */
    "level": UserLevel,
    /**
     * 用户密钥。必须为 32 个字符，仅可包含`0`到`9`和`a`到`f`之间的字符。
     */
    "secret": string
  }[]
}

export interface MTProtoOutbound {
    
}

/**
 * @inheritDoc https://www.v2ray.com/chapter_02/protocols/mtproto.html
 * ### MTProto
 * MTProto 是一个 Telegram 专用的代理协议。在 V2Ray 中可使用一组传入传出代理来完成 Telegram 数据的代理任务。    
 * 目前只支持转发到 Telegram 的 IPv4 地址。
 */
export interface MTProto {
  _name: 'mtproto'
  Inbound: MTProtoInbound
  Outbound: MTProtoOutbound
}