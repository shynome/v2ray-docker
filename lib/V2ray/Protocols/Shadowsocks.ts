import { Host, Port, StreamNetwork, UserLevel, Email } from "..";

export type CryptoMethod = 
  'aes-256-cfb' |
  'aes-128-cfb' |
  'chacha20' |
  'chacha20-ietf' |
  'aes-256-gcm' | 
  'aes-128-gcm' |
  'chacha20-poly1305' | 'chacha20-ietf-poly1305'

export interface ShadowsocksInbound {
  email?: Email
  /**
   * 加密方式，没有默认值。
   */
  method: CryptoMethod
  /**
   * 密码，任意字符串。Shadowsocks 协议不限制密码长度，但短密码会更可能被破解，建议使用 16 字符或更长的密码。
   */
  password: string
  /**
   *  (已过时，使用network替代): 是否开启 UDP 转发，默认值为 `false`。
   */
  udp?: boolean
  /**
   * 用户等级，默认值为 `0`。如果是自用的 VPS，可以设成 `1`。详见[本地策略](https://www.v2ray.com/chapter_02/policy.html)。
   */
  level?: UserLevel 
  /**
   * 是否强制 OTA，默认模式为自动，当指定了 `true` / `false` 时，则为强制不启用或启用。
   * - 当使用 AEAD 时，ota 设置无效
   */
  ota?: boolean
  network: StreamNetwork
}

export interface ShadowsocksOutbound {
  servers: {
    email?: Email
    address: Host,
    port: Port,
    method: CryptoMethod,
    password: string,
    /**
     * 是否开启 Shadowsocks 的一次验证（One time auth）。
     * - 当使用 `AEAD` 时，ota 设置无效
     */
    ota?: boolean,
    level: UserLevel
  }[]
}

/**
 * @inheritDoc https://www.v2ray.com/chapter_02/protocols/shadowsocks.html
 * ### Shadowsocks
 * [Shadowsocks](https://zh.wikipedia.org/wiki/Shadowsocks) 协议，包含传入和传出两部分，兼容大部分其它版本的实现。  
 * 具体哪些请看文档
 */
export class Shadowsocks {
  _name: 'shadowsocks'
  Inbound: ShadowsocksInbound
  Outbound: ShadowsocksOutbound
}