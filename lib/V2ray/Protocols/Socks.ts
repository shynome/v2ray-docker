import { Host, Port, UserLevel } from "..";

export interface SocksInbound {
  /**
   * Socks 协议的认证方式，支持`"noauth"`匿名方式和`"password"`用户密码方式。默认值为`"noauth"`。
   */
  auth?: 'noauth'|'password'
  /**
   * 一个数组，数组中每个元素为一个用户帐号，用户名由`user`指定，密码由`pass`指定。默认值为空。
   * - 当 `auth` 为 `password` 时有效。
   */
  accounts?: { user: string, pass: string }[]
  /**
   * 是否开启 UDP 协议的支持，`true` / `false`。默认值为 `false`。
   */
  udp?: boolean
  /**
   * 当开启 UDP 时，V2Ray 需要知道本机的 IP 地址。默认值为 `127.0.0.1`。
   */
  ip?: Host
  /**
   * (V2Ray 3.1 后等价于对应用户等级的 `connIdle` 策略): 从 Socks 客户端读取数据的超时设置（秒），0 表示不限时。默认值为 `300`。
   */
  timeout?: number
  userLevel?: UserLevel
}

export interface SocksOutbound {
  servers: {
    address: Host,
    port: Port,
    users: [
      {
        user: string
        pass: string
        level: UserLevel
      }
    ]
  }[]
}

/**
 * @inheritDoc https://www.v2ray.com/chapter_02/protocols/socks.html
 * ### Socks
 * 标准 Socks 协议实现，兼容 [Socks 4](http://ftp.icm.edu.pl/packages/socks/socks4/SOCKS4.protocol)、Socks 4a 和 [Socks 5](http://ftp.icm.edu.pl/packages/socks/socks4/SOCKS4.protocol)。
 */
export interface Socks {
  
  _name: 'socks'
  
  Inbound: SocksInbound

  Outbound: SocksOutbound

}