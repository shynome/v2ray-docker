import { Host, Port, UserLevel, Email, AlterId, Tag } from "..";

export type CryptoMethod = 'aes-128-cfb' | 'aes-128-gcm' | 'chacha20-poly1305' | 'auto' | 'none'

export interface VMessInbound {
  /**
   * 一组服务器认可的用户。clients 可以为空。当此配置用作动态端口时，V2Ray 会自动创建用户。
   */
 clients?: {
   /**
    * VMess 的用户 ID。
    */
   id: string,
   /**
    * 用户等级，详见[本地策略](https://www.v2ray.com/chapter_02/policy.html)
    */
   level?: UserLevel,
   /**
    * 为了进一步防止被探测，一个用户可以在主 ID 的基础上，再额外生成多个 ID。这里只需要指定额外的 ID 的数量，推荐值为 32。
    * 不指定的话，默认值是 0。最大值 65535。这个值不能超过服务器端所指定的值。
    */
   alterId?: AlterId,
   email?: Email
 }[]
 /**
  * 可选，clients 的默认配置
  */
 default?: {
   /**
    * 用户等级，意义同上。默认值为0。
    */
   level?: UserLevel,
   /**
    * 同 Inbound，默认值为64。
    */
   alterId?: AlterId
 }
 detour?: {
   to: Tag
 }
 /**
  * 禁止客户端使用不安全的加密方式，当客户端指定下列加密方式时，服务器会主动断开连接。默认值为`false`。
  * - none
  * - aes-128-cfb
  */
 disableInsecureEncryption?: boolean
}

export interface VMessOutbound {
  /**
   * 一个数组，包含一系列的服务器配置
   */
  vnext: {
    address: Host,
    port: Port,
    /**
     * 一组服务器认可的用户，其中每一个用户
     */
    users: {
      id: string
      alterId?: AlterId
      security: CryptoMethod
      level?: UserLevel
    }[]
  }[]
}

/**
 * @inheritDoc https://www.v2ray.com/chapter_02/protocols/vmess.html
 * ### Socks
 * [VMess](https://www.v2ray.com/eng/protocols/vmess.html) 是一个加密传输协议，它分为传入和传出两部分，通常作为 V2Ray 客户端和服务器之间的桥梁。
 */
export interface VMess {
   
  _name: 'vmess'

  Inbound: VMessInbound

  Outbound: VMessOutbound

}