
import { BlackholeOutbound } from "./Blackhole";
import { DokodemoDoorInbound } from "./DokodemoDoor";
import { FreedomOutbound } from "./Freedom";
import { HTTPInbound} from "./HTTP";
import { MTProtoOutbound, MTProtoInbound } from "./MTProto";
import { ShadowsocksOutbound, ShadowsocksInbound } from "./Shadowsocks";
import { SocksOutbound, SocksInbound } from "./Socks";
import { VMessOutbound, VMessInbound } from "./VMess";

export interface Outbound {
  'blackhole'     : BlackholeOutbound,
  'freedom'       : FreedomOutbound,
  'mtproto'       : MTProtoOutbound,
  'shadowsocks'   : ShadowsocksOutbound,
  'socks'         : SocksOutbound,
  'vmess'         : VMessOutbound,
}


export interface Inbound {
  'dokodemo-door' : DokodemoDoorInbound,
  'http'          : HTTPInbound,
  'mtproto'       : MTProtoInbound,
  'shadowsocks'   : ShadowsocksInbound,
  'socks'         : SocksInbound,
  'vmess'         : VMessInbound,
}
