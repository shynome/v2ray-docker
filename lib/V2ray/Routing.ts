import { Tag, Email, IP, StreamNetwork, Port } from ".";

export interface RoutingRule {
  /**
   * 目前只支持"field"。
   */
  type: "field",
  /**
   * 一个数组，数组每一项是一个域名的匹配。有四种形式：
   * - 纯字符串: 当此字符串匹配目标域名中任意部分，该规则生效。比如"sina.com"可以匹配"sina.com"、"sina.com.cn"和"www.sina.com"，但不匹配"sina.cn"。
   * - 正则表达式: 由"regexp:"开始，余下部分是一个正则表达式。当此正则表达式匹配目标域名时，该规则生效。例如"regexp:\\.goo.*\\.com$"匹配"www.google.com"、"fonts.googleapis.com"，但不匹配"google.com"。
   * - 子域名: 由"domain:"开始，余下部分是一个域名。当此域名是目标域名或其子域名时，该规则生效。例如"domain:v2ray.com"匹配"www.v2ray.com"、"v2ray.com"，但不匹配"xv2ray.com"。
   * - 特殊值"geosite:cn": 内置了一些[常见的国内网站域名](https://www.v2ray.com/links/chinasites/)。
   * - 特殊值"geosite:speedtest" (V2Ray 3.32+): Speedtest.net 的所有公用服务器列表。
   * - 从文件中加载域名: 形如"ext:file:tag"，必须以ext:（小写）开头，后面跟文件名和标签，文件存放在[资源目录](https://www.v2ray.com/chapter_02/env.html#asset-location)中，文件格式与geosite.dat相同，标签必须在文件中存在。
   */
  domain?: string[]
  /**
   * 一个数组，数组内每一个元素代表一个 IP 范围。当某一元素匹配目标 IP 时，此规则生效。有三种形式：
   * - IP: 形如"127.0.0.1"。
   * - [CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing)
   * - GeoIP: 形如`"geoip:cn"`，必须以geoip:（小写）开头，后面跟双字符国家代码，支持几乎所有可以上网的国家。
   * - 特殊值："geoip:private" (V2Ray 3.5+)，包含所有私有地址，如127.0.0.1。
   */
  ip?: IP[]
  /**
   * 端口范围，有两种形式：
   * - `"a-b"`: a 和 b 均为正整数，且小于 65536。这个范围是一个前后闭合区间，当目标端口落在此范围内时，此规则生效。
   * - `a`: a 为正整数，且小于 65536。当目标端口为 a 时，此规则生效。
   */
  port?: Port
  /**
   *  可选的值有"tcp"、"udp"或"tcp,udp"，当连接方式是指定的方式时，此规则生效。
   */
  network?: StreamNetwork
  /**
   * 一个数组，数组内每一个元素是一个 IP 或 CIDR。当某一元素匹配来源 IP 时，此规则生效。
   */
  source?:IP[]
  /**
   * 一个数组，数组内每一个元素是一个邮箱地址。当某一元素匹配来源用户时，此规则生效。当前 Shadowsocks 和 VMess 支持此规则。
   */
  user?: Email[]
  /**
   * 一个数组，数组内每一个元素是一个标识。当某一元素匹配传入协议的标识时，此规则生效。
   */
  inboundTag?: Tag[]
  /**
   * 一个数组，数组内每一个元素表示一种协议。当某一个协议匹配当前连接的流量时，此规则生效。
   * - 可选的值有`"http"`、`"tls"`、`"bittorrent"`，均为小写。
   */
  protocol?: 'http'|'tls'|'bittorrent'[]
  /**
   *  对应一个[额外传出连接配置](https://www.v2ray.com/chapter_02/02_protocols.html)的标识。
   */
  outboundTag?: Tag
}

export interface Routing {
  strategy: 'rules'
  settings: {
    domainStrategy?: 'AsIs' | 'IPIfNonMatch'
    rules: RoutingRule[]
  }
}