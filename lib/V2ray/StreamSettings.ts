import { Path, Host } from ".";

export declare namespace Stream {
  interface tcp {
    /**数据包头部伪装设置 */
    header?: {
      /**
       * 伪装类型
       */
      type?: 'none'|'http'
    }
  }
  /**
   * @inheritDoc https://www.v2ray.com/chapter_02/transport/mkcp.html
   */
  interface kcp {
    /**
     * 最大传输单元（maximum transmission unit），
     * 请选择一个介于 `576` - `1460` 之间的值。
     * 默认值为 `1350`
     */
    mtu?: number
    /**
     * 传输时间间隔（transmission time interval），单位毫秒（ms），
     * mKCP 将以这个时间频率发送数据。请选译一个介于 `10` - `100` 之间的值。
     * 默认值为 `50`
     */
    tti?: number
    /**
     * 行链路容量，即主机发出数据所用的最大带宽，单位 MB/s，默认值 `5`。
     * - 注意是 Byte 而非 bit;
     * - 可以设置为 0，表示一个非常小的带宽
     */
    uplinkCapacity?: number
    /**
     * 下行链路容量，即主机接收数据所用的最大带宽，单位 MB/s，默认值 `20`。
     * - 注意是 Byte 而非 bit;
     * - 可以设置为 0，表示一个非常小的带宽
     */
    downlinkCapacity?: number
    /**
     * 是否启用拥塞控制，默认值为 `false`。
     * - 开启拥塞控制之后，V2Ray 会自动监测网络质量，当丢包严重时，会自动降低吞吐量；当网络畅通时，也会适当增加吞吐量。
     */
    congestion?: boolean
    /**
     * 单个连接的读取缓冲区大小，单位是 MB。默认值为 `2`。
     */
    readBufferSize?:number
    /**
     * 单个连接的写入缓冲区大小，单位是 MB。默认值为 `2`。
     */
    writeBufferSize?:number
    /**数据包头部伪装设置 */
    header?: {
      /**
       * 伪装类型，可选的值有：
       * - `"none"`: 默认值，不进行伪装，发送的数据是没有特征的数据包。
       * - `"srtp"`: 伪装成 SRTP 数据包，会被识别为视频通话数据（如 FaceTime）。
       * - `"utp"`:  伪装成 uTP 数据包，会被识别为 BT 下载数据。
       * - `"wechat-video"`: 伪装成微信视频通话的数据包。
       * - `"dtls"` (V2Ray 3.24+): 伪装成 DTLS 1.2 数据包。
       */
      type?: 'none'|'srtp'|'utp'|'wechat-video'|'dtls'
    }
  }

  /**
   * @inheritDoc https://www.v2ray.com/chapter_02/transport/websocket.html
   */
  interface ws {
    /**
     * WebSocket 所使用的 HTTP 协议路径，默认值为 `""`。
     */
    path?: Path
    /**
     * 自定义 HTTP 头，一个键值对，每个键表示一个 HTTP 头的名称，对应的值是字符串。默认值为空。
     */
    headers?: {
      [key: string]: string
    }
  }

  /**
   * @inheritDoc https://www.v2ray.com/chapter_02/transport/h2.html
   * ### HTTP/2 传输方式
   * V2Ray 3.17 中加入了基于 HTTP/2 的传输方式。它完整按照 HTTP/2 标准实现，可以通过其它的 HTTP 服务器（如 Nginx）进行中转。
   * 
   * ##### 小贴士
   * - 客户端和服务器必须同时开启 TLS 才可以正常使用这个传输方式。
   * - 此传输方式目前尚在测试阶段，很有可能有各种问题，也不排除之后会修改配置的可能性。
   */
  interface http {
    /**
     * 一个字符串数组，每一个元素是一个域名。客户端会随机从列表中选出一个域名进行通信，服务器会验证域名是否在列表中。
     */
    host?: Host[]
    /**
     * HTTP 路径。客户端和服务器必须一致。
     */
    path?: Path
  }
  
  /**
   * @inheritDoc https://www.v2ray.com/chapter_02/transport/domainsocket.html
   * 
   * ### DomainSocket 传输方式
   * Domain Socket 使用标准的 Unix domain socket 来传输数据。它的优势是使用了操作系统内建的传输通道，而不会占用网络缓存。相比起本地环回网络（local loopback）来说，Domain socket 速度略快一些。   
   * 目前仅可用于支持 Unix domain socket 的平台，如 macOS 和 Linux。在 Windows 上不可用。
   * 
   * ##### 小贴士
   * - 如果指定了 domain socket 作为传输方式，在传入传出代理中配置的端口和 IP 地址将会失效，所有的传输由 domain socket 取代。
   */
  interface ds {
    /**
     * 一个合法的文件路径。在运行 V2Ray 之前，这个文件必须不存在。
     */
    path: Path
  }
  
}

export interface tlsSettings {
  security: 'tls'
  tlsSettings: {
    allowInsecure?: boolean
    serverName?: string
  }
}

export interface StreamAllSettings {
  tcp           : { tcpSettings   : Stream.tcp    }
  kcp           : { kcpSettings   : Stream.kcp    }
  ws            : { wsSettings    : Stream.ws     }
  http          : { httpSettings  : Stream.http   }
  h2            : { httpSettings  : Stream.http   }
  domainsocket  : { dsSettings    : Stream.ds     }
}

export type StreamSettingKey = keyof StreamAllSettings

export type StreamSettings<N extends StreamSettingKey> = { network: N } & Partial<tlsSettings> & StreamAllSettings[N]

