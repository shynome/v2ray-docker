
/**连接协议名称 */
export type Protocol = 'socks'|'vmess'|'shadowsocks'|'http'|'dokodemo-door'|'freedom'

export type Network = 'ws'|'h2'|'tcp'

export type StreamNetwork = 'tcp'|'udp'|'tcp,udp'

export type Path = string

export type Tag = string

/**域名或者`ip` */
export type Host = string

export type Port = number

/**`Host` + `Port` */
export type Hostname = string

export type IP = string

export type UserLevel = number

export type AlterId = number

export type Email = string

import { StreamSettings } from './StreamSettings'

import { Inbound } from "./Inbound";

import { Outbound } from './Outbound'

import { Routing } from "./Routing";
import { Api } from "./Api";
import { Log } from "./Log";
import { DNS } from "./DNS";
import { Stats } from "./Stats";
import { Policy } from "./Policy";

export {
  Inbound,
  Outbound,
  StreamSettings,
  Routing,
}

export interface Config {
  log: Log
  api?: Api
  dns?: DNS
  stats?: Stats
  routing: Routing
  policy?: Policy
  inbound: Inbound<any,any>
  inboundDetour: Inbound<any,any>[]
  outbound: Outbound<any>
  outboundDetour: Outbound<any>[]
  transport?: {}
}

