export as namespace V2rayN

import { StreamSettingKey, Stream } from "../V2ray/StreamSettings";

export interface VMessConfig {
  /**
   * version
   */
  v:string
  /**
   * remark
   */
  ps:string
  /**
   * server address
   */
  add:string
  port:string
  /**
   * VMess_ID
   */
  id:string
  /**
   * alertid
   */
  aid:string
  /**
   * network
   */
  net: 'ws'|'kcp'|'tcp'|'h2'
  /**
   * 伪装类型
   */
  type: string
  /**
   * 伪装的域名
   * - http host中间逗号(,)隔开
   * - ws host
   * - h2 host
   */
  host:string
  /**
   * ws/h2 stream path
   */
  path:string
  /**
   * tls
   */
  tls:'tls'
}

export type Protocol = 'vmess'|'ss'
