import { Outbound } from "../V2ray";
import { Outbound as ProtocolOutbound } from "../V2ray/Protocols";
import { StreamAllSettings, StreamSettings, tlsSettings } from "../V2ray/StreamSettings";
import { parse as UrlParse } from "url";
import { env } from "..";
import {  } from "./V2rayN";

export function parse (url:string) {
  let [ protocol, args ] = url.split('://') as ['ss'|'vmess',string]
  let config
  switch(protocol){
    case 'ss': {
      let [ base64str, remark ] = args.split('#')
      let url = protocol+'://'+Buffer.from(base64str,'base64').toString()
      let conf = UrlParse(url)
      let [ crypto_method, password ] = conf.auth.split(':')
      config = {
        'protocol': 'shadowsocks',
        'settings': {
          'servers':[
            {
              'address': conf.hostname,
              'port': Number(conf.port),
              'method': crypto_method,
              "password": password,
              'ota': false,
              'level': env.LOCAL_AREA_NETWORK ? 1 : 0
            }
          ]
        },
        mux:{
          enabled: false
        },
        'streamSettings': {
          'network': 'tcp'
        }
      } as Outbound<'shadowsocks','tcp'>
      break
    }
    case 'vmess':{
      args = Buffer.from(args,'base64').toString()
      let vmess_conf:V2rayN.VMessConfig = JSON.parse(args)

      let vmess_next:ProtocolOutbound['vmess'] = {
        'vnext':[{
          'address':vmess_conf.add,
          'port': Number(vmess_conf.port),
          'users':[
            {
              'id':vmess_conf.id,
              'security':'auto',
              'alterId': Number(vmess_conf.aid),
            }
          ]
        }]
      }

      let mux_enable = env.MUX_ENABLE===''?false:true
      let stream_settings
      switch(vmess_conf.net){
        case 'ws': {
          stream_settings = {
            'network': 'ws',
            'wsSettings': {
              'connectionReuse': true,
              'path': vmess_conf.path,
              'headers': {
                'Host': vmess_conf.host
              }
            }
          } as StreamSettings<'ws'>
          mux_enable = false
        break }
        case 'kcp': {
          stream_settings = {
            'network': 'kcp',
            'kcpSettings':{
              'header':{
                'type': vmess_conf.type || null
              }
            }
          } as StreamSettings<'kcp'>
        break }
        case 'tcp': {
          stream_settings = {
            'network': 'tcp',
            'tcpSettings': {
            },
          } as StreamSettings<'tcp'>
        break }
        case 'h2': {
          stream_settings = {
            network: 'h2',
            httpSettings: {
              path: vmess_conf.path,
              host: vmess_conf.host.split(',')
            }
          } as StreamSettings<'h2'>
        break }
      }

      if(vmess_conf.tls==='tls'){
        let tls_setting:tlsSettings = {
          security: vmess_conf.tls,
          tlsSettings: {
            allowInsecure: false,
            serverName: null,
          }
        }
        stream_settings = {
          ...stream_settings,
          ...tls_setting
        }
      }
      
      config = {

        protocol: 'vmess',
        settings: vmess_next,
        streamSettings: stream_settings,
        mux:{
          enabled: mux_enable,
        }
        
      } as Outbound<'vmess'>

      break
    }
    default:
      throw new Error(`链接协议不可导入, 不可导入的链接协议是: ${protocol}`)
  }
  return config
}

export function export2url(config:Outbound<keyof ProtocolOutbound>){
  let share_url = ''
  switch(config.protocol){
    case "shadowsocks": {
      
      break
    }
    case 'vmess': {
      break
    }
    default:
      throw new Error(`the ${config.protocol} protocol is unsupport export to url`)
  }
  return share_url 
}
