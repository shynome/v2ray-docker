
import fs = require('fs');
import { isDev, env } from ".";
import { Config } from "./V2ray";

const config_path = isDev ? 'test.config.json' : '/etc/v2ray/config.json'
env.LOG_LEVEL = env.LOG_LEVEL || 'warning'

export async function main(config_filepath=config_path){
  
  let config: Config = {} as any

  switch(true){

    case !!(env.PROXY_PORTS && env.PROXY_HOSTNAME):
      // @ts-ignore
      config = await (await import('./tpl/proxy')).default(env)
      break;

    case !!env.SERVER_NAME:
      if(env.TLS_CRT && env.TLS_KEY){
        // to next case
      }else{
        console.log(`enable https proxy, env [ TLS_CRT, TLS_KEY ] is required.`)
        return process.exit(1)
      }

    default:
      // @ts-ignore
      config = await (await import('./tpl')).default(env)
      break;
  }

  let ports = config.inboundDetour.concat(config.inbound).map(({ protocol='', port, streamSettings:{ network='' }={} })=>{
    return {
      port,
      protocol:[protocol,network].filter(k=>k).join('-'),
    }
  })

  console.log('you have start those ports: ')
  console.log(JSON.stringify(ports, null, 2))
  
  // console.log(config)
  fs.writeFileSync(
    config_filepath,
    JSON.stringify(config, null, 2,)
  )

}
