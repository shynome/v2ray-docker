import {} from './Env'

export const isDev = !/production/i.test(process.env.NODE_ENV)

export const env = new Proxy(process.env as Env,{
  // @ts-ignore
  get(target,key){ return target[key] || '' }
})