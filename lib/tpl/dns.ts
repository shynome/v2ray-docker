export default async (env:Env)=>({
  "servers": [
    "8.8.8.8",
    "8.8.4.4",
    "localhost"
  ]
})