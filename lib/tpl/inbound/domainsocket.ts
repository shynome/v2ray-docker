// 未完成

import { Inbound } from "../../V2ray/";

export default async (env:Env,path='/var/run/v2ray.sock'):Promise<Inbound<'http','http'>>=>({
  port: 0,
  protocol: 'http',
  'settings': {
  },
  'streamSettings':{
    // @ts-ignore
    'network': 'domainsocket',
    'dsSettings':{
      'path':path
    },
    'httpSettings':{
    }
  }
})
