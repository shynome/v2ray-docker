
export default async (env:Env,port=3009)=>({
  "port": port,
  "protocol": "http",
  "settings": {
    "timeout": 0,
    "allowTransparent": false,
    "userLevel": 0,
    "accounts": [
      {
        "user": "v2ray",
        "pass": env.PASSWORD
      }
    ]
  }
})