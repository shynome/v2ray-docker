
export default async (env:Env,port=3003)=>({
  "port": port,
  "protocol": "http",
  "settings": {
    "timeout": 0,
    "allowTransparent": false,
    "userLevel": 0,
    "accounts": [
      {
        "user": "v2ray",
        "pass": env.PASSWORD
      }
    ]
  },
  "streamSettings":{
    "network": "tcp",
    "serverName": env.SERVER_NAME,
    "security": "tls",
    "tlsSettings": {
      "certificates": [
        {
          "certificate": env.TLS_CRT.split('\n'),
          "key": env.TLS_KEY.split('\n'),
        }
      ]
    }
  }
})