
import wsStreamInit from "../streamSettings/kcp";

export default async (env:Env,port=3005)=>({
  "port": port, // 服务器监听端口
  "protocol": "vmess",    // 主传入协议
  "settings": {
    "clients": [
      {
        "id": env.VMESS_ID,  // 用户 ID，客户端与服务器必须相同
        "alterId": 64
      }
    ]
  },
  "streamSettings": await wsStreamInit(env)
})
