
export default async (env:Env,port=3001)=>({
  "protocol": "shadowsocks",
  "port": port,
  "settings": {
    "method": "aes-256-cfb",
    "ota": false,
    "password": env.PASSWORD
  }
})