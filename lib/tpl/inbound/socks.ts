

export default async (env:Env,port=3004)=>({
  "protocol": "socks",
  "port": port,
  "settings": {
    "accounts": [
      { "user": "v2ray", "password": env.PASSWORD }
    ]
  }
})