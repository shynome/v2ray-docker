import { Config } from "../V2ray";

import *as inbound from "./inbound";

export default async (env:Env):Promise<Config>=>({

  "log" : {
    "access": "",
    "error": "",
    "loglevel": env.LOG_LEVEL || 'warning'
  },

  "inbound":  {
    "protocol": "socks",
    "port": 3000,
    "settings": {
      "auth":"noauth",
      "udp": true,
    }
  },
  "inboundDetour":[

    ...(
      env.PASSWORD 
      ? [

        await inbound.shadowsocks(env,3001),

        // https proxy 会被 dns 污染
        env.SERVER_NAME && await inbound.https(env,3002),

        await inbound.socks(env,3003),

        // http proxy 必定被 GFW 拦下
        await inbound.http(env,3009),
          
      ] 
      : [] 
    ),

    ...(
      env.VMESS_ID 
      ? [

        await inbound.vmess(env,3004),

        await inbound.ws(env,3005),

        await inbound.kcp(env,3006),

      ] 
      : []
    ),
    
  ].filter(k=>k),

  ...(await (await import('./outbound')).default(env)),

  "dns": await (await import('./dns')).default(env),

  "routing": await (await import('./routing')).default(env)

})