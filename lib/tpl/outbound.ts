import { Outbound } from "../V2ray";
import *as V2rayN from "../V2rayN";

export default async (env:Env):Promise<{ outbound:Outbound<any,any>, outboundDetour: Outbound<any,any>[] }>=>{
  let outbound:Outbound<any> = {
    "protocol": "freedom",
    "settings": {
      
    },
  } as Outbound<'freedom'>
  if(env.VNEXT){
    outbound = {
      "tag": "agentout",
      ...V2rayN.config.parse(env.VNEXT)
    }
  }
  return {
    outbound,
    "outboundDetour": [
      {
        "protocol": "freedom",
        "settings": {
          "response": null
        },
        "tag": "direct"
      },
      {
        "protocol": "blackhole",
        "settings": {
          "response": {
            "type": "http"
          }
        },
        "tag": "blockout"
      }
    ],
  }
}