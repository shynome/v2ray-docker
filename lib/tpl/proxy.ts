

export default async (env:Env)=>{
  let { PROXY_PORTS, PROXY_HOSTNAME } = env
  let ports = /**@type {string[]}*/([])

  if(/-/.test(PROXY_PORTS)){
    let [ start, end ] = PROXY_PORTS.split('-').map(n=>Number(n)).sort()
    while(start<=end){
      ports.push(`${start++}`)
    }
  }else{
    ports = PROXY_PORTS.split(',')
  }
  
  let inboundDetour = ports.map(v=>Number(v)).map(port=>({
    "protocol": "dokodemo-door",
    "port": port,
    "settings": {
      "address": PROXY_HOSTNAME,
      "port": port,
      "network": "tcp,udp",
      "timeout": 0
    }
  }))

  return {
    ...(await require('.')(env)),
    inboundDetour,
  }
  
}