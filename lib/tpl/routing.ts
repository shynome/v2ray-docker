

import https = require('https');
import { RoutingRule, Routing } from "../V2ray/Routing";

const get = (url:string):Promise<string>=>new Promise((rl,rj,s='')=>{
  return https.get(url,res=>{
    res
    .setEncoding('utf8')
    .on('data',d=>s+=d).on('end', ()=>rl(s))
    .once('error',rj)
  })
})

export default async (env:Env):Promise<Routing>=>{

  let LocalAreaNetworkRules:RoutingRule[] = [
    {
      "type": "field",
      "ip": [
        "0.0.0.0/8",
        "10.0.0.0/8",
        "100.64.0.0/10",
        "127.0.0.0/8",
        "169.254.0.0/16",
        "172.16.0.0/12",
        "192.0.0.0/24",
        "192.0.2.0/24",
        "192.168.0.0/16",
        "198.18.0.0/15",
        "198.51.100.0/24",
        "203.0.113.0/24",
        "::1/128",
        "fc00::/7",
        "fe80::/10"
      ],
      "outboundTag": env.LOCAL_AREA_NETWORK ? "direct" : "blockout"
    }
  ]

  let ChinaDirectLinkRules:RoutingRule[] = [
    {
      "type": "field",
      // @ts-ignore
      "port": null,
      "outboundTag": "direct",
      // @ts-ignore
      "ip": null,
      "domain": [
        "geosite:cn"
      ]
    },
    {
      "type": "field",
      // @ts-ignore
      "port": null,
      "outboundTag": "direct",
      "ip": [
        "geoip:cn"
      ],
      // @ts-ignore
      "domain": null
    }
  ]
  
  let rules = LocalAreaNetworkRules
  
  let myip = await get('https://myip.ipip.net')
  
  if(env.VNEXT && /中国/.test(myip)){
    rules = ChinaDirectLinkRules.concat(LocalAreaNetworkRules)
  }
  
  return {
    "strategy": "rules",
    "settings": {
      "domainStrategy": "IPIfNonMatch",
      rules,
    }
  }
}
