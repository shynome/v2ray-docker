import { StreamSettings } from "../../V2ray/StreamSettings";


export default async (env:Env):Promise<StreamSettings<"kcp">>=>({
  network: "kcp",
  kcpSettings: {
    header: { type: "wechat-video" },
  },
})