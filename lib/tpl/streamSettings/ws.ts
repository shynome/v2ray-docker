
export default async (env:Env)=>({
  "network":"ws",
  ...(
    env.PROXY_PATH
    ? { "wsSettings":{ "path": env.PROXY_PATH } }
    : {}
  )
})