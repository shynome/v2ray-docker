
import { parse, export2url } from "../../lib/V2rayN/config";
import deepfilter = require('deep-filter')
import assert = require('assert')
1
function filterNullValue(obj:any){
  return deepfilter(obj,(val:any,prop:string)=>{
    switch(true){
      case val===null:
      case typeof val==='object' && Object.keys(val).length===0: 
      case Array.isArray(val) && val.length===0:
      case typeof val==='string' && val.length ===0:
      case ['email','tag','level','kcpSettings'].includes(prop):
        return false
      default:
        return true
    }
  })
}

describe('config',()=>{

  let test_data:[string,string,any][] = [
    [ 'ws',
      'vmess://ew0KICAidiI6ICIyIiwNCiAgInBzIjogImRyb25lLndzbC5mdW4t5Ymv5pysIiwNCiAgImFkZCI6ICJiYWlkdS5jb20iLA0KICAicG9ydCI6ICI0NDQ0NCIsDQogICJpZCI6ICJhYTE4ZDNhOS05NjI3LTQ5ZmItOWU5Yy0wYmZkMzAzMDc0ZGYiLA0KICAiYWlkIjogIjY0IiwNCiAgIm5ldCI6ICJ3cyIsDQogICJ0eXBlIjogIm5vbmUiLA0KICAiaG9zdCI6ICIiLA0KICAicGF0aCI6ICIvcmF5IiwNCiAgInRscyI6ICJ0bHMiDQp9',
      {
        "tag": "agentout",
        "protocol": "vmess",
        "settings": {
          "vnext": [
            {
              "address": "baidu.com",
              "port": 44444,
              "users": [
                {
                  "id": "aa18d3a9-9627-49fb-9e9c-0bfd303074df",
                  "alterId": 64,
                  "email": "t@t.tt",
                  "security": "auto"
                }
              ]
            }
          ],
          "servers": null
        },
        "streamSettings": {
          "network": "ws",
          "security": "tls",
          "tlsSettings": {
            "allowInsecure": false,
            "serverName": null
          },
          "tcpSettings": null,
          "kcpSettings": null,
          "wsSettings": {
            "connectionReuse": true,
            "path": "/ray",
            "headers": null
          },
          "httpSettings": null
        },
        "mux": {
          "enabled": false
        }
      }
    ],
    [ 'tcp',
      'vmess://ew0KICAidiI6ICIyIiwNCiAgInBzIjogImRyb25lLndzbC5mdW4t5Ymv5pysIiwNCiAgImFkZCI6ICJiYWlkdS5jb20iLA0KICAicG9ydCI6ICI0NDQ0NCIsDQogICJpZCI6ICJhYTE4ZDNhOS05NjI3LTQ5ZmItOWU5Yy0wYmZkMzAzMDc0ZGYiLA0KICAiYWlkIjogIjY0IiwNCiAgIm5ldCI6ICJ0Y3AiLA0KICAidHlwZSI6ICJub25lIiwNCiAgImhvc3QiOiAiIiwNCiAgInBhdGgiOiAiL3JheSIsDQogICJ0bHMiOiAiIg0KfQ==',
      {
        "tag": "agentout",
        "protocol": "vmess",
        "settings": {
          "vnext": [
            {
              "address": "baidu.com",
              "port": 44444,
              "users": [
                {
                  "id": "aa18d3a9-9627-49fb-9e9c-0bfd303074df",
                  "alterId": 64,
                  "email": "t@t.tt",
                  "security": "auto"
                }
              ]
            }
          ],
          "servers": null
        },
        "streamSettings": {
          "network": "tcp",
          "security": "",
          "tlsSettings": null,
          "tcpSettings": null,
          "kcpSettings": null,
          "wsSettings": null,
          "httpSettings": null
        },
        "mux": {
          "enabled": false
        }
      },    
    ],
    [ 'kcp',
      'vmess://ew0KICAidiI6ICIyIiwNCiAgInBzIjogImRyb25lLndzbC5mdW4t5Ymv5pysIiwNCiAgImFkZCI6ICJiYWlkdS5jb20iLA0KICAicG9ydCI6ICI0NDQ0NCIsDQogICJpZCI6ICJhYTE4ZDNhOS05NjI3LTQ5ZmItOWU5Yy0wYmZkMzAzMDc0ZGYiLA0KICAiYWlkIjogIjY0IiwNCiAgIm5ldCI6ICJrY3AiLA0KICAidHlwZSI6ICJub25lIiwNCiAgImhvc3QiOiAiYS5jb20sYi5jb20iLA0KICAicGF0aCI6ICIvcmF5IiwNCiAgInRscyI6ICIiDQp9',
      {
        "tag": "agentout",
        "protocol": "vmess",
        "settings": {
          "vnext": [
            {
              "address": "baidu.com",
              "port": 44444,
              "users": [
                {
                  "id": "aa18d3a9-9627-49fb-9e9c-0bfd303074df",
                  "alterId": 64,
                  "email": "t@t.tt",
                  "security": "auto"
                }
              ]
            }
          ],
          "servers": null
        },
        "streamSettings": {
          "network": "kcp",
          "security": "",
          "tlsSettings": null,
          "tcpSettings": null,
          "kcpSettings": {
            "mtu": 1350,
            "tti": 50,
            "uplinkCapacity": 12,
            "downlinkCapacity": 100,
            "congestion": false,
            "readBufferSize": 2,
            "writeBufferSize": 2,
            "header": {
              "type": "none",
              "request": null,
              "response": null
            }
          },
          "wsSettings": null,
          "httpSettings": null
        },
        "mux": {
          "enabled": false
        }
      },
    ],
    [ 'h2',
      'vmess://ew0KICAidiI6ICIyIiwNCiAgInBzIjogImRyb25lLndzbC5mdW4t5Ymv5pysIiwNCiAgImFkZCI6ICJiYWlkdS5jb20iLA0KICAicG9ydCI6ICI0NDQ0NCIsDQogICJpZCI6ICJhYTE4ZDNhOS05NjI3LTQ5ZmItOWU5Yy0wYmZkMzAzMDc0ZGYiLA0KICAiYWlkIjogIjY0IiwNCiAgIm5ldCI6ICJoMiIsDQogICJ0eXBlIjogIm5vbmUiLA0KICAiaG9zdCI6ICJhLmNvbSxiLmNvbSIsDQogICJwYXRoIjogIi9yYXkiLA0KICAidGxzIjogInRscyINCn0=',
      {
        "tag": "agentout",
        "protocol": "vmess",
        "settings": {
          "vnext": [
            {
              "address": "baidu.com",
              "port": 44444,
              "users": [
                {
                  "id": "aa18d3a9-9627-49fb-9e9c-0bfd303074df",
                  "alterId": 64,
                  "email": "t@t.tt",
                  "security": "auto"
                }
              ]
            }
          ],
          "servers": null
        },
        "streamSettings": {
          "network": "h2",
          "security": "tls",
          "tlsSettings": {
            "allowInsecure": false,
            "serverName": null
          },
          "tcpSettings": null,
          "kcpSettings": null,
          "wsSettings": null,
          "httpSettings": {
            "path": "/ray",
            "host": [
              "a.com",
              "b.com"
            ]
          }
        },
        "mux": {
          "enabled": false
        }
      },
    ],
    [ 'shadowsocks',
      'ss://YWVzLTI1Ni1jZmI6ZHNmZHdyaXVoaWVmcmVvcXJmQGJhaWR1LmNvbTo1NTU1#xxxx',
      {
        "tag": "agentout",
        "protocol": "shadowsocks",
        "settings": {
          "vnext": null,
          "servers": [
            {
              "email": "v2rayn@v2ray.com",
              "address": "baidu.com",
              "method": "aes-256-cfb",
              "ota": false,
              "password": "dsfdwriuhiefreoqrf",
              "port": 5555,
              "level": 1
            }
          ]
        },
        "streamSettings": {
          "network": "tcp",
          "security": null,
          "tlsSettings": null,
          "tcpSettings": null,
          "kcpSettings": null,
          "wsSettings": null,
          "httpSettings": null
        },
        "mux": {
          "enabled": false
        }
      }
    ]
  ]
  test_data = test_data.map((arr)=>{
    arr[2] = filterNullValue(arr[2])
    return arr
  })
  
  describe('parse',()=>{
    for(let [name,url,config] of test_data){
      it(name,()=>{
        let parsed_config = filterNullValue(parse(url))
        assert.deepStrictEqual(
          parsed_config,
          config,
          JSON.stringify([config,parsed_config],null,2)
        )
      })
    }
  })
  
  describe('export2url',()=>{
    for(let [name,url,config] of test_data){
      break
      it(name,()=>{
        export2url(config)
      })
    }
  })

})